package;

import com.junkbyte.console.Cc;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.Lib;
import ru.row.App;
import starling.core.Starling;

/**
 * ...
 * @author 
 */
class Main {
    public static var starling:Starling;
	
	static function main() {
		var stage = Lib.current.stage;
		stage.scaleMode = StageScaleMode.NO_SCALE;
		stage.align = StageAlign.TOP_LEFT;
		// Entry point
        
        Cc.startOnStage(stage, " ");
        Cc.commandLine = true;
        
        starling = new Starling(App, stage);
        starling.start();
	}
}
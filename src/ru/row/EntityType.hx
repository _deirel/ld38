package ru.row;
import ru.row.SRandom.RandomTemplate;

/**
 * ...
 * @author 
 */
class EntityType {

    public static var Player:EntityType = new EntityType("player", EntityKind.Player, null, 10).mcd(1.0).acd(1.0).HP("100").baseStats("4+2d3", 10, 10, 10);
    public static var Portal:EntityType = new EntityType("portal", EntityKind.Portal, 0, 0).HP("0");
    public static var Heal:EntityType = new EntityType("heal", EntityKind.Heal, 26, 0).HP("0");
    public static var Cat:EntityType = _m("cat", -10, 10).mcd(2).acd(0.5).HP("18+2d2").rw(35).baseStats("1d5", 0, 0, 0);
    public static var Minotaur:EntityType = _m("minotaur", -10, 2).mcd(2.5).acd(2).HP("80+2d5").rw(150).baseStats("2d5", 0, 0, 0);
    public static var Tree_0:EntityType = _d("tree_0", 0, 10);
    public static var Tree_1:EntityType = _d("tree_1", -10, 13);
    public static var Tree_2:EntityType = _d("tree_2", 0, 10);
    public static var Rock_0:EntityType = _d("rock_0", 7, 18);
    public static var Rock_1:EntityType = _d("rock_1", 13, 16);
    
    public static var Monsters:Array<EntityType> = [];
    public static var Decorations:Array<EntityType> = [];
    
    private static function _m(id:String, ?pivotX:Float, ?pivotY:Float):EntityType {
        var t = new EntityType(id, EntityKind.Monster, pivotX, pivotY);
        Monsters.push(t);
        return t;
    }
    
    private static function _d(id:String, ?pivotX:Float, ?pivotY:Float):EntityType {
        var t = new EntityType(id, EntityKind.Decoration, pivotX, pivotY);
        Decorations.push(t);
        return t;
    }
    
    public var type:String;
    public var kind:EntityKind;
    public var pivotX:Null<Float>;
    public var pivotY:Null<Float>;
    public var moveCoolDown:Float = 1.0;
    public var attackCoolDown:Float = 1.0;
    public var hp:RandomTemplate = [DV(100)];
    public var reward:Float = 0.0;
    public var damage:RandomTemplate = [DV(0)];
    public var stamina:Float = 0.0;
    public var strength:Float = 0.0;
    public var speed:Float = 0.0;
    
    public function new(type:String, kind:EntityKind, ?pivotX:Float, ?pivotY:Float) {
        this.type = type;
        this.kind = kind;
        this.pivotX = pivotX;
        this.pivotY = pivotY;
    }
    
    public function mcd(value:Float):EntityType {
        this.moveCoolDown = value;
        return this;
    }
    
    public function acd(value:Float):EntityType {
        this.attackCoolDown = value;
        return this;
    }
    
    public function HP(template:String):EntityType {
        this.hp = SRandom.fromString(template);
        return this;
    }
    
    public function rw(value:Float):EntityType {
        this.reward = value;
        return this;
    }
    
    public function baseStats(damage:String, stamina:Float, strength:Float, speed:Float):EntityType {
        this.damage = SRandom.fromString(damage);
        this.stamina = stamina;
        this.strength = strength;
        this.speed = speed;
        return this;
    }
}

enum EntityKind {
    Player;
    Portal;
    Heal;
    Monster;
    Decoration;
}
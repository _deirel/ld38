package ru.row;

/**
 * ...
 * @author 
 */
class TimeMachine {
    public var timeScale(get, never):Float;
    
    private var _normalCoolDown:Float;
    private var _player:Entity;
    
    public function new(player:Entity, normalCoolDown:Float) {
        _player = player;
        _normalCoolDown = normalCoolDown;
    }
    
    private function get_timeScale() return _player.lastAction != null ? _player.lastAction.coolDown / _normalCoolDown : 0.0;
    
    public function dispose():Void {
        _player = null;
    }
}
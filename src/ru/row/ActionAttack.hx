package ru.row;
import ru.row.Entity;

/**
 * ...
 * @author 
 */
class ActionAttack extends Action {
    
    public function new(enemyId:String, attacker:Entity) {
        super();
		this._kind = ActionKind.Attack(enemyId);
        this._coolDown = attacker.getStats().getAttackCooldown();
    }
    
    override public function execute(entity:Entity):Void {
        switch (_kind) {
            case ActionKind.Attack(enemyId):
                var enemy:Entity = entity.world.getEntity(enemyId);
                if (enemy != null) {
                    var damage = entity.getStats().getDamage();
                    enemy.hp -= damage;
                }
            default:
        }
    }
}
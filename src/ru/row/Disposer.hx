package ru.row;

/**
 * ...
 * @author 
 */
class Disposer {
    private var _items:Array<Dynamic> = [];
    
    public function new() {
    }
    
    public function add(item:Dynamic):Void {
        if (Reflect.hasField(item, "dispose")) {
            _items.push(item);
        }
    }
    
    public function dispose():Void {
        for (i in _items) {
            Reflect.callMethod(i, Reflect.field(i, "dispose"), []);
        }
        _items = [];
    }
}
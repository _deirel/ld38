package ru.row;
import starling.display.DisplayObject;

/**
 * ...
 * @author 
 */
class Camera {
    public var x(get, set):Float;
    public var y(get, set):Float;
    
    private var _target:IPositionable;
    private var _screen:IScreenSizeProvider;
    private var _x:Float;
    private var _y:Float;
    
    public function new(target:IPositionable, screenSizeProvider:IScreenSizeProvider) {
        _target = target; 
        _screen = screenSizeProvider;
        this.x = 0.0;
        this.y = 0.0;
    }
    
    private function get_x() return _x;
    
    private function get_y() return _y;
    
    private function set_x(value:Float):Float {
        _x = value;
        _target.setX(_screen.getScreenWidth() / 2 - _x);
        return _x;
    }
    
    private function set_y(value:Float):Float {
        _y = value;
        _target.setY(_screen.getScreenHeight() / 2 - _y);
        return _y;
    }
}
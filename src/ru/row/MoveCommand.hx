package ru.row;
import feathers.controls.NumericStepper;
import flash.Lib;
import motion.Actuate;
import motion.easing.Linear;

/**
 * ...
 * @author 
 */
class MoveCommand {
    private var _dx:Int;
    private var _dy:Int;
    
    private var _executing:Bool = false;
    private var _targetX:Float;
    private var _targetY:Float;
    private var _startX:Float;
    private var _startY:Float;
    private var _entity:Entity;
    
    private var _duration:Float;
    private var _time:Float;
    
    private var _doneCallback:Float -> Void;
    
    public function new(duration:Float, dx:Int, dy:Int, doneCb:Float -> Void) {
        _duration = duration;
        _doneCallback = doneCb;
        _dx = dx;
        _dy = dy;
    }
    
    public function execute(entity:Entity, prevDt:Float = 0.0):Void {
        _entity = entity;
        _startX = _entity.x;
        _startY = _entity.y;
        _targetX = _entity.x += _dx;
        _targetY = _entity.y += _dy;
        _time = prevDt;
        _executing = true;
    }
    
    public function update(dt:Float):Void {
        if (_executing) {
            _time += dt;
            var p = _time / _duration;
            _entity.realtimeX = p * _targetX + (1 - p) * _startX;
            _entity.realtimeY = p * _targetY + (1 - p) * _startY;
            if (p >= 1) {
                _executing = false;
                _doneCallback(p - 1);
            }
        }
    }
    
    public function getMovement() return { dx: -_dx, dy: -_dy };
    
    public function dispose():Void {
        _executing = false;
        _entity = null;
        _doneCallback = null;
    }
}
package ru.row;
import com.junkbyte.console.Cc;
import flash.Lib;
import flash.errors.Error;
import haxe.io.StringInput;
import msignal.Signal.Signal1;
import msignal.Signal.Signal2;
import pathfinder.Coordinate;
import pathfinder.Pathfinder;
import ru.row.EntityType.EntityKind;
import ru.row.TileMap.ShiftResult;
import ru.row.Util.PathPoint;
import ru.row.Util.Vec2i;

using Lambda;

/**
 * ...
 * @author 
 */
class World {
    public var onEntityAdded:Signal1<Entity> = new Signal1();
    public var onEntityDestroy:Signal1<Entity> = new Signal1();
    public var onShift:Signal2<ShiftResult, Movement> = new Signal2();
    public var onLevelChange:Signal2<World, Int> = new Signal2();
    
    private var _tileMap:TileMap;
    
    private var _size:Int;
    private var _offsetX:Int = 0;
    private var _offsetY:Int = 0;
    private var _generator:MapGenerator;
    
    private var _player:Entity;
    private var _entities:Map<String, Entity> = new Map();
    
    private var _entitiesToDestroy:Map<String, Bool> = new Map();
    
    private var _timeMachine:TimeMachine;
    private var _entityFactory:EntityFactory;
    
    private var _pathMap:PathMap;
    private var _pathFinder:Pathfinder;
    
    private var _expSystem:EXPSystem;
    
    private var _level:Int = 1;
    
    public function new(size:Int, generator:MapGenerator, entityFactory:EntityFactory) {
        _size = size;
        _generator = generator;
        _generator.world = this;
        _entityFactory = entityFactory;
        _tileMap = new TileMap(_size, _generator);
        _player = _entityFactory.create(EntityType.Player, Std.int((_size - 1) / 2), Std.int((_size - 1) / 2));
        _player.world = this;
        _player.calcStats();
        _entities[_player.id] = _player;
        _player.onDeath.add(playerDeathHandler);
        _timeMachine = new TimeMachine(_player, 0.5);
        
        _player.onBeginAction.add(playerBeginActionHandler);
        
        _pathMap = new PathMap(this);
        _pathFinder = new Pathfinder(_pathMap);
        
        _expSystem = new EXPSystem();
    }
    
    public function addEntity(e:Entity):Void {
        e.world = this;
        e.calcStats();
        _entities[e.id] = e;
        if (e.isInteractive()) {
            e.onDeath.add(creatureDeathHandler);
        }
        onEntityAdded.dispatch(e);
    }
    
    private function creatureDeathHandler(creature:Entity):Void {
        _expSystem.add(creature.getStats().getKillExperience());
        _entitiesToDestroy[creature.id] = true;
    }
    
    private function playerDeathHandler(_):Void {
        clearEntities();
        _expSystem.resetEXP();
        _player.revive();
        changeLevel( -1);
    }
    
    private function playerBeginActionHandler(action:Action):Void {
        switch (action.kind) {
            case ActionKind.Move(dx, dy): this.shift({ dx: -dx, dy: -dy });
            default:
        }
    }

    public function shift(movement:Movement):Void {
        var prevOffsetX = _offsetX;
        var prevOffsetY = _offsetY;
        var monsters = getMonsterByPositionMap();
        _offsetX -= movement.dx;
        _offsetY -= movement.dy;
        var shiftResult = _tileMap.shift(movement);
        _generator.completeTiles(shiftResult.unattached, monsters);
        _generator.completeEntitites(shiftResult.unattached);
        var entitiesToCreate:Array<Entity> = [];
        for (t in shiftResult.unattached) {
            var globalFromX = t.from.x + prevOffsetX;
            var globalFromY = t.from.y + prevOffsetY;
            var globalToX = t.to.x + _offsetX;
            var globalToY = t.to.y + _offsetY;
            for (e in _entities) {
                if (e.x == globalFromX && e.y == globalFromY) {
                    _entitiesToDestroy[e.id] = true;
                }
            }
            if (t.targetEntity != null) {
                entitiesToCreate.push(switch (t.targetEntity) {
                    case Id(type): _entityFactory.create(type, globalToX, globalToY);
                    case Instance(e): e.clone(_entityFactory, globalToX, globalToY);
                });
            }
        }
        for (e in entitiesToCreate) {
            addEntity(e);
        }
        onShift.dispatch(shiftResult, movement);
    }
    
    private var _pathFromEntity:Entity = null;
    private var _pathToEntity:Entity = null;
    
    public function isWalkable(x:Int, y:Int):Bool {
        for (e in _entities) {
            var localX = e.x - _offsetX;
            var localY = e.y - _offsetY;
            if (!e.inSamePlace(_pathFromEntity) && !e.inSamePlace(_pathToEntity) && x == localX && y == localY) {
                return false;
            }
        }
        return true;
    }
    
    public function getPath(fromX:Int, fromY:Int, toX:Int, toY:Int):Array<PathPoint> {
        _pathFromEntity = getEntityByPosition(fromX, fromY);
        _pathToEntity = getEntityByPosition(toX, toY);
        if (_pathToEntity != null && !_pathToEntity.isInteractive()) {
            return [];
        }
        var path = null;
        try {
            path = _pathFinder.createPath(
                new Coordinate(fromX - _offsetX, fromY - _offsetY),
                new Coordinate(toX - _offsetX, toY - _offsetY),
                null,
                true,
                true
            );
        } catch (err:Error) {}
        if (path == null) {
            return [];
        }
        var result = path.map(function (coord) return { 
            x: coord.x, 
            y: coord.y,
            monster: (_pathFromEntity != null && _pathFromEntity.x == coord.x + _offsetX && _pathFromEntity.y == coord.y + _offsetY) ? _pathFromEntity.id :
                     (_pathToEntity   != null && _pathToEntity.x   == coord.x + _offsetX && _pathToEntity.y   == coord.y + _offsetY) ? _pathToEntity.id : null
        });
        _pathFromEntity = _pathToEntity = null;
        return result;
    }
    
    public function getPathFromPlayer(toX:Int, toY:Int):Array<PathPoint> {
        return getPath(_player.x, _player.y, toX, toY);
    }
    
    public function getPathToPlayer(fromX:Int, fromY:Int):Array<PathPoint> {
        return getPath(fromX, fromY, _player.x, _player.y);
    }
    
    public function getEntityByPosition(x:Int, y:Int):Entity {
        for (e in _entities) if (e.x == x && e.y == y) return e;
        return null;
    }
    
    public function update(dt:Float):Void {
        var gameDt = dt * _timeMachine.timeScale;
        for (e in _entities) {
            e.affectCoolDown(gameDt);
        }
        
        var wasDestroying:Bool = false;
        for (id in _entitiesToDestroy.keys()) {
            destroyEntity(id);
            wasDestroying = true;
        }
        if (wasDestroying) {
            _entitiesToDestroy = new Map();
        }
    }
    
    private function destroyEntity(id:String):Void {
        var e = _entities[id];
        if (e != null) {
            onEntityDestroy.dispatch(e);
            _entities.remove(id);
            e.dispose();
        }
    }
    
    public function getMonsterByPositionMap():Map<String,Entity> {
        var map:Map<String,Entity> = new Map();
        for (e in _entities) switch (e.type.kind) {
            case EntityKind.Monster: map['${e.x - _offsetX}:${e.y - _offsetY}'] = e;
            default:
        }
        return map;
    }
    
    public function upgradeLevel():Void {
        clearEntities();
        changeLevel(1);
    }
    
    private function clearEntities():Void {
        for (e in _entities) if (!e.isPlayer()) _entitiesToDestroy[e.id] = true;
    }
    
    public function changeLevel(delta:Int):Void {
        if (_level + delta < 1) {
            return;
        }
        _level += delta;
        onLevelChange.dispatch(this, delta);
    }
    
    public function getEnemyCount():Int return Lambda.count(_entities, function (e) return e != _player && e.isCreature());
    
    public function getTiles():Array<Tile> return _tileMap.getTiles();
    
    public function getSize():Int return _size;
    
    public function getOffsetX():Int return _offsetX;
    
    public function getOffsetY():Int return _offsetY;
    
    public function getPlayer():Entity return _player;
    
    public function getEntities():Map<String, Entity> return _entities;
    
    public function getEntity(id:String):Entity return _entities[id];
    
    public function getTimeScale():Float return _timeMachine.timeScale;
    
    public function getEXPSystem():EXPSystem return _expSystem;
    
    public function getLevel():Int return _level;
}
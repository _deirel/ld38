package ru.row;

/**
 * ...
 * @author 
 */
class Tile {
    public var x:Int;
    public var y:Int;
    public var id:String;
    
    public function new(x:Int, y:Int, id:String) {
        this.x = x;
        this.y = y;
        this.id = id;
    }
    
    public function clone():Tile {
        return new Tile(x, y, id);
    }
}
package ru.row;
import ru.row.Action;

/**
 * ...
 * @author 
 */
class GlobalGoalMoveNextTile extends GlobalGoal {
    private var _flag:Bool = false;
    private var _dx:Int;
    private var _dy:Int;
    
    public function new(dx:Int, dy:Int) {
        super();
        _dx = dx;
        _dy = dy;
    }
    
    override public function getNextAction():Action {
        _flag = !_flag;
        //return _flag ? new ActionMoveNextTile(Math.random() > 0.5 ? 1 : -1, Math.random() > 0.5 ? 1 : 0) : null;
        return _flag ? new ActionMoveNextTile(_dx, _dy) : null;
    }
}
package ru.row;
import msignal.Signal.Signal1;

/**
 * ...
 * @author 
 */
class ActionView {
    private var _entity:EntityView;
    
    public function new(entity:EntityView) {
        _entity = entity;
    }
    
    public function setProgress(p:Float):Void {
    }
    
    public function end():Void {
    }
    
    public function dispose():Void {
        _entity = null;
    }
}
package ru.row;

/**
 * @author 
 */
interface IScreenSizeProvider {
    function getScreenWidth():Int;
    function getScreenHeight():Int;
}
package ru.row;
import flash.display.BitmapData;
import starling.display.Image;
import starling.textures.Texture;
import starling.textures.TextureAtlas;
import flash.utils.ByteArray;
import flash.xml.XML;

/**
 * ...
 * @author 
 */
class TileGraphicsProvider {
    private static var _atlas:TextureAtlas;
    
    private static function getAtlas() {
        if (_atlas == null) {
            var xml = new XML(new TilesAtlasXML());
            _atlas = new TextureAtlas(Texture.fromBitmapData(new TilesAtlasPNG()), xml);
        }
        return _atlas;
    }
    
    public function new() {
    }
    
    public function getGraphics(id:String):Image {
        var atlas = getAtlas();
        return new Image(getAtlas().getTexture(id));
    }
    
    public static function getPathMarkerTexture():Texture {
        return getAtlas().getTexture("path");
    }
    
    public static function getBGTexture():Texture return getAtlas().getTexture("bg");
    
    public static function getCloudsTexture():Texture return getAtlas().getTexture("clouds");
}

@:file("assets/tiles.xml") class TilesAtlasXML extends ByteArray {}
@:bitmap("assets/tiles.png") class TilesAtlasPNG extends BitmapData { public function new() super(0, 0); }
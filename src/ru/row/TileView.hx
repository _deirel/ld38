package ru.row;
import flash.display.BitmapData;
import haxe.io.Path;
import motion.Actuate;
import motion.easing.Linear;
import ru.row.TileView.PathMarker;
import starling.display.Image;
import starling.display.Sprite;
import starling.textures.Texture;

/**
 * ...
 * @author 
 */
class TileView extends Sprite {
    private static var _graphicsProvider:TileGraphicsProvider = new TileGraphicsProvider();
    
    private var _isChanging:Bool = false;
    private var _image:Image;
    private var _pathMarker:PathMarker;
    
    public function new(tileId:String) {
        super();
        _image = createImage(tileId);
        addChild(_image);
        _pathMarker = new PathMarker();
        touchGroup = true;
    }
    
    private inline function createImage(id:String):Image {
        var image = _graphicsProvider.getGraphics(id);
        image.pivotX = 64;
        image.pivotY = image.height - 37;
        return image;
    }
    
    public function changeTo(id:String, time:Float):Void {
        var newImage = createImage(id);
        newImage.alpha = 0.0;
        addChild(newImage);
        Actuate.tween(_image, time, { alpha: 0.0 }).ease(Linear.easeNone).onComplete(imageFadeOutHandler, [_image]);
        _image = newImage;
        Actuate.tween(_image, time, { alpha: 1.0 }).ease(Linear.easeNone);
    }
    
    private function imageFadeOutHandler(image:Image):Void {
        image.removeFromParent(true);
    }
    
    public function showPathMarker(visible:Bool, ?isMonster:Bool):Void {
        if (visible){ 
            if(_pathMarker.parent == null) {
                addChild(_pathMarker);
            }
            _pathMarker.color = isMonster ? 0xff0000 : 0xffffff;
        } else if (!visible && _pathMarker.parent != null) {
            _pathMarker.removeFromParent(false);
        }
    }
    
    override public function dispose():Void {
        if (_pathMarker != null) {
            _pathMarker.removeFromParent(true);
            _pathMarker = null;
        }
        _image = null;
        super.dispose();
    }
}

class PathMarker extends Image {
    
    public function new() {
        super(TileGraphicsProvider.getPathMarkerTexture());
        this.pivotX = width / 2;
        this.pivotY = height / 2 + 10;
    }
}
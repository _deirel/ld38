package ru.row;

/**
 * ...
 * @author 
 */
class ActionViewAttack extends ActionView {
    public static inline var HitMaxTime = 0.2;
    public static inline var HitPart = 0.2;
    
    private var _enemy:Entity;
    private var _startX:Float;
    private var _startY:Float;
    private var _targetX:Float;
    private var _targetY:Float;
    private var _targetOffsetY:Float;
    private var _hitPart:Float;
    
    public function new(entity:EntityView, enemy:Entity) {
        super(entity);
        _enemy = enemy;
        _startX = _entity.realtimeX;
        _startY = _entity.realtimeY;
        _targetX = 0.6 * _startX + 0.4 * _enemy.x;
        _targetY = 0.6 * _startY + 0.4 * _enemy.y;
        _targetOffsetY = -20.0;
        var coolDown = entity.getEntity().type.attackCoolDown;
        var hitDuration = coolDown * HitPart;
        _hitPart = hitDuration > HitMaxTime ? HitMaxTime / coolDown : HitPart;
    }
    
    override public function setProgress(p:Float):Void {
        var p1:Float;
        if (p < _hitPart) {
            p1 = p / _hitPart;
            _entity.realtimeX = p1 * _targetX + (1 - p1) * _startX;
            _entity.realtimeY = p1 * _targetY + (1 - p1) * _startY;
            _entity.setImageYOffset(Math.sqrt(p1) * _targetOffsetY);
        } else {
            p1 = (p - _hitPart) / (1 - _hitPart);
            _entity.realtimeX = p1 * _startX + (1 - p1) * _targetX;
            _entity.realtimeY = p1 * _startY + (1 - p1) * _targetY;
            _entity.setImageYOffset(Math.sqrt(1 - p1) * _targetOffsetY);
        }
    }
    
    override public function end():Void {
        _entity.realtimeX = _startX;
        _entity.realtimeY = _startY;
        _entity.setImageYOffset(0.0);
        super.end();
    }
}
package ru.row;
import flash.utils.ByteArray;
import msignal.Signal.Signal0;
import msignal.Signal.Signal1;
import ru.row.StatsView.StatPlateView;
import starling.display.DisplayObjectContainer;
import starling.text.TextField;
import ru.row.Stat;
using ru.row.Util;

/**
 * ...
 * @author asdfsafd
 */
class StatsView extends BaseView {
    public var onClose:Signal0 = new Signal0();
    
    private var _world:World;
    private var _statPlates:Map<Stat,StatPlateView>;

    public function new(world:World) {
        super();
        graphics = App.uiBuilder.create(EmbedJson.parse(new StatsJson()));
        
        _world = world;
        
        var btnClose = _d(new SimpleButton(graphics.ch("btnClose")));
        btnClose.onClick.add(btnClickHandler);
        
        _statPlates = new Map();
        addStatPlate(Stamina, "health");
        addStatPlate(Speed, "speed");
        addStatPlate(Strength, "strength");
        
        updateValues();
    }
    
    private function addStatPlate(statType:Stat, containerName:String):Void {
        var statPlate = _d(new StatPlateView(statType, graphics.cc(containerName)));
        _statPlates[statType] = statPlate;
        statPlate.onBuyClick.add(statBuyClickHandler);
    }
    
    private function statBuyClickHandler(stat:Stat):Void {
        var stats = _world.getPlayer().getStats();
        if (_world.getEXPSystem().transaction(stats.getStatUpgradeCost(stat))) {
            stats.upgradeStat(stat);
            updateValues();
        }
    }
    
    private function btnClickHandler(_):Void {
        onClose.dispatch();
    }
    
    private function updateValues():Void {
        var player = _world.getPlayer();
        for (statType in [Stamina, Speed, Strength]) {
            var statPlate = _statPlates[statType];
            statPlate.setValue(player.getStats().getStatValue(statType));
            statPlate.setCost(player.getStats().getStatUpgradeCost(statType));
        }
    }
    
    override public function dispose():Void {
        _statPlates = null;
        _world = null;
        onClose.removeAll();
        onClose = null;
        super.dispose();
    }
}

class StatPlateView {
    public var onBuyClick:Signal1<Stat> = new Signal1();
    
    private var _stat:Stat;
    private var _txtValue:TextField;
    private var _btnUpgrade:SimpleButton;
    private var _txtBonus:TextField;
    private var _txtCost:TextField;
    
    public function new (stat:Stat, container:DisplayObjectContainer) {
        _stat = stat;
        _txtValue = container.ct("label");
        _txtBonus = container.ct("bonus");
        _txtCost = container.cc("btnBuy").ct("cost");
        _btnUpgrade = new SimpleButton(container.cc("btnBuy"));
        _btnUpgrade.onClick.add(buyClickHandler);
    }
    
    private function buyClickHandler(_) {
        onBuyClick.dispatch(_stat);
    }
    
    public function setValue(value:Float):Void {
        _txtValue.text = Std.string(Std.int(value));
    }
    
    public function setCost(value:Float):Void {
        _txtCost.text = Std.string(Std.int(value));
    }
    
    public function dispose():Void {
        onBuyClick.removeAll();
        onBuyClick = null;
        _btnUpgrade.dispose();
        _btnUpgrade = null;
        _txtBonus = _txtCost = _txtValue = null;
    }
}

@:file("assets/stats.json") class StatsJson extends ByteArray {}
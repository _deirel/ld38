package ru.row;

using Lambda;

/**
 * ...
 * @author 
 */
class TileMap {
    private var _size:Int;
    private var _tiles:Array<Tile> = [];
    private var _generator:MapGenerator;
    
    public function new(size:Int, generator:MapGenerator) {
        _size = size;
        _generator = generator;
        _tiles = [for (x in 0..._size) for (y in 0..._size) null];
        
        var startArea = _generator.createStartArea(_size);
        for (tile in startArea) _tiles[_key(tile.x, tile.y)] = tile;
    }

    public function shift(movement:Movement):ShiftResult {
        var unattachedTiles:Array<Tile> = [];
        var attachedTiles:Array<Tile> = [];
        var attachedIds:Array<Bool> = [for (i in 0..._tiles.length) false];
        var attachedTransitions:Array<TileTransition> = [];
        var unattachedTransitions:Array<TileTransition> = [];
        for (i in 0..._tiles.length) {
            var tile = _tiles[i];
            tile.x += movement.dx;
            tile.y += movement.dy;
            if (!isInRange(tile.x, tile.y)) {
                unattachedTiles.push(tile);
            } else {
                attachedTiles.push(tile);
                attachedIds[_key(tile.x, tile.y)] = true;
                attachedTransitions.push({
                    from: { x: tile.x - movement.dx, y: tile.y - movement.dy },
                    to: { x: tile.x, y: tile.y },
                    reattach: false,
                    targetTileId: tile.id
                });
            }
        }
        for (tile in attachedTiles) _tiles[_key(tile.x, tile.y)] = tile;
        for (tile in unattachedTiles) unattachedTransitions.push(findTileNewPlace(tile, attachedIds, movement));
        return {
            attached: attachedTransitions,
            unattached: unattachedTransitions
        };
    }
    
    private function findTileNewPlace(tile:Tile, attachedIds:Array<Bool>, movement:Movement):TileTransition {
        var x = tile.x;
        var y = tile.y;
        var transition:TileTransition = null;
        do {
            x -= movement.dx;
            y -= movement.dy;
            if (isInRange(x, y)) {
                if (!attachedIds[_key(x, y)]) {
                    transition = {
                        from: { x: tile.x - movement.dx, y: tile.y - movement.dy },
                        to: { x: x, y: y },
                        reattach: true,
                        sourceTileId: tile.id
                    };
                }
            } else break;
        } while (transition == null);
        
        if (transition != null) {
            tile.x = x;
            tile.y = y;
            _tiles[_key(x, y)] = tile;
        }
        
        return transition;
    }
    
    private inline function isInRange(x:Int, y:Int):Bool return !(x < 0 || y < 0 || x >= _size || y >= _size);
    
    private inline function getLocalTile(x:Int, y:Int):Tile {
        return _tiles[_key(x, y)];
    }
    
    private inline function _key(x:Int, y:Int):Int return y * _size + x;
    
    public function getTiles():Array<Tile> return _tiles;
}

typedef ShiftResult = {
    var attached:Array<TileTransition>;
    var unattached:Array<TileTransition>;
}
package ru.row;
import com.junkbyte.console.Cc;
import ru.row.Action;

/**
 * ...
 * @author 
 */
class GlobalGoalAttack extends GlobalGoal {
    private var _world:World;
    private var _entity:Entity;
    private var _target:Entity;
    private var _alreadyAttacked:Bool = false;
    
    public function new(world:World, entity:Entity, target:Entity) {
        super();
		_world = world;
        _entity = entity;
        _target = target;
    }
    
    override public function getNextAction():Action {
        if (_target == null) {
            return null;
        }
        if (_entity.isNear(_target)) {
            if (!_alreadyAttacked) {
                _alreadyAttacked = true;
                return new ActionAttack(_target.id, _entity);
            } else {
                return null;
            }
        } else {
            var path = _world.getPath(_entity.x, _entity.y, _target.x, _target.y);
            if (path.length > 1) {
                var p0 = path[0];
                var p1 = path[1];
                return new ActionMoveNextTile(p1.x - p0.x, p1.y - p0.y, _entity);
            }
        }
        return null;
    }
    
    override public function dispose():Void {
        _world = null;
        _entity = null;
        super.dispose();
    }
}
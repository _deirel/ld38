package ru.row;
import msignal.Signal.Signal1;

/**
 * ...
 * @author asdfsafd
 */
class EXPSystem {
    public var exp(get, never):Float;
    public var onChange:Signal1<EXPSystem> = new Signal1();
    
    private var _exp:Float = 0.0;
    
    public function new() {
    }
    
    private function get_exp() return _exp;
    
    public function transaction(cost:Float):Bool {
        if (cost <= _exp) {
            _exp -= cost;
            onChange.dispatch(this);
            return true;
        }
        return false;
    }
    
    public function add(value:Float):Void {
        _exp += value;
        onChange.dispatch(this);
    }
    
    public function resetEXP():Void {
        if (_exp != 0.0) {
            _exp = 0.0;
            onChange.dispatch(this);
        }
    }
    
    public function dispose():Void {
        onChange.removeAll();
        onChange = null;
    }
}
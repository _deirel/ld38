package ru.row;

/**
 * @author 
 */
interface IPositionable {
    function setX(value:Float):Void;
    function setY(value:Float):Void;
}
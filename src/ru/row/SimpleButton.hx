package ru.row;
import msignal.Signal.Signal1;
import starling.display.DisplayObject;
import starling.display.DisplayObjectContainer;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

/**
 * ...
 * @author 
 */
class SimpleButton {
    public var onClick:Signal1<SimpleButton> = new Signal1();
    
    private var _target:DisplayObject;
    private var _lastY:Float;
    
    public function new(target:DisplayObject) {
        _target = target;
        if (Std.is(_target, DisplayObjectContainer)) {
            (cast _target:DisplayObjectContainer).touchGroup = true;
        }
        _target.useHandCursor = true;
        _target.addEventListener(TouchEvent.TOUCH, touchHandler);
    }
    
    private function touchHandler(e:TouchEvent):Void {
        var touch = e.getTouch(_target, TouchPhase.BEGAN);
        if (touch != null) {
            onPress();
            return;
        }
        
        touch = e.getTouch(_target, TouchPhase.ENDED);
        if (touch != null) {
            onRelease();
            onClick.dispatch(this);
        }
    }
    
    private function onPress():Void {
        _lastY = _target.y;
        _target.y += 5;
    }
    
    private function onRelease():Void {
        _target.y = _lastY;
    }
    
    public function dispose():Void {
        if (_target != null) {
            _target.removeEventListener(TouchEvent.TOUCH, touchHandler);
            _target = null;
        }
        if (onClick != null) {
            onClick.removeAll();
            onClick = null;
        }
    }
}
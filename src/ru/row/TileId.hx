package ru.row;

/**
 * ...
 * @author 
 */
class TileId {

    public static inline var Nothing:String = "-";
    public static var Grass_0:String = _("grass_0");
    public static var Grass_1:String = _("grass_1");
    public static var Grass_2:String = _("grass_2");
    public static var Grass_3:String = _("grass_0");
    public static var Grass_4:String = _("grass_1");
    public static var Grass_5:String = _("grass_0");
    public static var Grass_6:String = _("grass_0");
    public static var Grass_7:String = _("grass_1");
    public static var Grass_8:String = _("grass_2");
    public static var Soil_0:String = _("soil_0");
    public static var Soil_1:String = _("soil_1");
    public static var Soil_2:String = _("soil_2");
    public static var Grass_10:String = _("grass_0");
    public static var Grass_11:String = _("grass_1");
    public static var Grass_12:String = _("grass_0");
    public static var Grass_13:String = _("grass_0");
    public static var Grass_14:String = _("grass_1");
    public static var Grass_15:String = _("grass_2");
    public static var Grass_16:String = _("grass_0");
    public static var Grass_17:String = _("grass_1");
    public static var Grass_18:String = _("grass_2");
    public static var Soil_10:String = _("soil_0");
    public static var Soil_11:String = _("soil_1");
    public static var Soil_12:String = _("soil_2");
    public static var Soil_13:String = _("soil_3");
    
    public static var All:Array<String> = [];
    public static var Count:Int = 0;
    
    private static function _(id:String):String {
        Count = All.push(id);
        return id;
    }
}
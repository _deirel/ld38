package ru.row;

/**
 * ...
 * @author 
 */
class EntityHeal extends EntityCreature {

    public function new(id:String, x:Int, y:Int) {
        super(id, EntityType.Heal, x, y);
    }
    
    override function getNewGlobalGoal():GlobalGoal {
        return null;
    }
    
    override function onDeathHandler():Void {
        world.getPlayer().hp += world.getPlayer().getStats().getHeal();
        super.onDeathHandler();
    }
}
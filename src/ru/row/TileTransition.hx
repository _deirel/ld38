package ru.row;
import ru.row.MapGenerator.EntityInstance;

/**
 * ...
 * @author 
 */
typedef TileTransition = {
    var from:TilePoint;
    var to:TilePoint;
    var reattach:Bool;
    @:optional var sourceTileId:String;
    @:optional var targetTileId:String;
    @:optional var targetEntity:EntityInstance;
}

typedef TilePoint = {
    var x:Int;
    var y:Int;
}
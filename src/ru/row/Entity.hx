package ru.row;
import msignal.Signal.Signal0;
import msignal.Signal.Signal1;
/**
 * ...
 * @author 
 */
class Entity {
    public var lastAction(get, never):Action;
    
    public var onBeginAction:Signal1<Action> = new Signal1();
    public var onEndAction:Signal1<Action> = new Signal1();
    
    public var onHPChanged:Signal1<Entity> = new Signal1();
    public var onDeath:Signal1<Entity> = new Signal1();
    
    public var id:String;
    public var type:EntityType;
    
    public var x:Int;
    public var y:Int;
    
    public var world:World;
    
    public var hp(get, set):Float;
    private var _hp:Float;
    
    private var _isDead:Bool = false;
    
    public var maxHP:Float;
    public var baseHP:Float;
    
    private var _lastAction:Action;
    private var _globalGoal:GlobalGoal;
    
    private var _coolDownElapsed:Float = 0.0;
    
    private var _stats:Stats;
    
    public function new(id:String, type:EntityType, x:Int, y:Int) {
        this.id = id;
        this.type = type;
        this.x = x;
        this.y = y;
        this.baseHP = SRandom.getInt(type.hp);
        _stats = new Stats(this, type.damage, type.stamina, type.strength, type.speed, isPlayer());
    }
    
    public function calcStats():Void {
        this.maxHP = this._hp = _stats.getHP();
    }
    
    public function revive():Void {
        this.hp = maxHP;
        this._isDead = false;
    }
    
    public function isPlayer() return switch (type.kind) {
        case Player: true;
        default: false;
    };
    
    public function affectCoolDown(dt:Float):Void {
        _coolDownElapsed -= dt;
        if (_coolDownElapsed <= 0.0) {
            _coolDownElapsed = 0.0;
            tryToBeginNextAction();
            if (_globalGoal == null) {
                setGlobalGoal(getNewGlobalGoal());
            }
        }
    }
    
    private function getNewGlobalGoal():GlobalGoal {
        return null;
    }
    
    public function setGlobalGoal(goal:GlobalGoal):Void {
        _globalGoal = goal;
        if (_globalGoal != null && _lastAction == null) {
            tryToBeginNextAction();
        }
    }
    
    private function tryToBeginNextAction():Void {
        if (_lastAction != null) {
            onEndAction.dispatch(_lastAction);
            _lastAction.dispose();
        }
        
        _lastAction = _globalGoal != null ? _globalGoal.getNextAction() : null;
        
        if (_lastAction != null) {
            _coolDownElapsed = _lastAction.coolDown;
            _lastAction.execute(this);
            onBeginAction.dispatch(_lastAction);
        } else {
            _globalGoal = null;
        }
    }
    
    public function isCreature():Bool return switch (type.kind) {
        case Monster | Player: true;
        default: false;
    }
    
    public function isInteractive():Bool return switch (type.kind) {
        case Monster | Portal | Player | Heal: true;
        default: false;
    }
    
    public function get_lastAction() return _lastAction;
    
    public function getCooldownElapsed() return _coolDownElapsed;
    
    public function isNear(e:Entity):Bool {
        return (Math.abs(x - e.x) <= 1 && Math.abs(y - e.y) <= 1);
    }
    
    public function inSamePlace(e:Entity):Bool {
        return e != null ? x == e.x && y == e.y : false;
    }
    
    public function get_hp() return _hp;
    
    public function set_hp(value:Float) {
        if (_hp != value) {
            if (value > maxHP) {
                value = maxHP;
            }
            _hp = value;
            if (_hp <= 0) {
                _hp = 0.0;
                if (!_isDead) {
                    _isDead = true;
                    onHPChanged.dispatch(this);
                    onDeathHandler();
                }
            } else {
                onHPChanged.dispatch(this);
            }
        }
        return _hp;
    }
    
    private function onDeathHandler():Void {
        onDeath.dispatch(this);
    }
    
    public function getStats():Stats return _stats;
    
    public function clone(factory:EntityFactory, x:Int, y:Int):Entity {
        var copy = factory.create(type, x, y);
        
        copy.maxHP = maxHP;
        copy._hp = _hp;
        
        copy._stats.dispose();
        copy._stats = _stats.clone(copy);
        
        return copy;
    }
    
    public function dispose():Void {
        _stats.dispose();
        _stats = null;
        onEndAction.removeAll();
        onEndAction = null;
        onBeginAction.removeAll();
        onBeginAction = null;
        onHPChanged.removeAll();
        onHPChanged = null;
        onDeath.removeAll();
        onDeath = null;
        if (_lastAction != null) {
            _lastAction.dispose();
            _lastAction = null;
        }
        if (_globalGoal != null) {
            _globalGoal.dispose();
            _globalGoal = null;
        }
    }
}
package ru.row;
import haxe.EnumTools.EnumValueTools;
import ru.row.Entity;

/**
 * ...
 * @author 
 */
class ActionMoveNextTile extends Action {

    public function new(dx:Int, dy:Int, entity:Entity) {
        super();
        this._kind = ActionKind.Move(dx, dy);
        this._coolDown = entity.getStats().getMoveCooldown();
    }
    
    override public function execute(entity:Entity):Void {
        switch (_kind) {
            case ActionKind.Move(dx, dy):
                entity.x += dx;
                entity.y += dy;
            default:
        }
    }
}
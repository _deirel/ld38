package ru.row;
import starling.display.DisplayObject;

/**
 * ...
 * @author asdfsafd
 */
class BaseMediator<ViewClass:BaseView> {
    private var _view:ViewClass;
    
    public function new(view:ViewClass) {
        _view = view;
    }
    
    public function pause() _view.graphics.touchable = false;
    
    public function resume() _view.graphics.touchable = true;
    
    public function dispose():Void {
        if (_view != null) {
            _view.dispose();
            _view = null;
        }
    }
}
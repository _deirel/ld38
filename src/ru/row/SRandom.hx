package ru.row;

/**
 * ...
 * @author 
 */
class SRandom {

    static public function getInt(expr:RandomTemplate):Int {
        var sum = 0;
        for (e in expr) sum += switch (e) {
            case RV(mul, rng): mul * (Std.int(Math.random() * rng) + 1);
            case DV(val): val;
        }
        return sum;
    }
    
    static public function fromString(tpl:String):RandomTemplate {
        return tpl.split("+").map(function (etpl) {
            var id = etpl.indexOf("d");
            if (id > -1) {
                return RV(Std.parseInt(etpl.substr(0, id)), Std.parseInt(etpl.substr(id + 1)));
            } else {
                return DV(Std.parseInt(etpl));
            }
        });
    }
}

typedef RandomTemplate = Array<RandomTemplateElement>

enum RandomTemplateElement {
    /** 1d2 и т.п. */
    RV(mul:Int, rng:Int);
    
    /** Константное значение */
    DV(val:Int);
}
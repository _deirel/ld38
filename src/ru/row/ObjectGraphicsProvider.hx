package ru.row;
import flash.utils.ByteArray;
import starling.display.Image;
import starling.textures.Texture;
import flash.display.BitmapData;
import starling.textures.TextureAtlas;
import flash.xml.XML;

/**
 * ...
 * @author 
 */
class ObjectGraphicsProvider {
    private static var _atlas:TextureAtlas;
    
    private static function getAtlas() {
        if (_atlas == null) {
            var xml = new XML(new EntitiesAtlasXML());
            _atlas = new TextureAtlas(Texture.fromBitmapData(new EntitiesAtlasPNG()), xml);
        }
        return _atlas;
    }
    
    public function new() {
    }
    
    public function getGraphics(id:String):Image {
        var atlas = getAtlas();
        return new Image(getAtlas().getTexture(id));
    }
}

@:file("assets/entities.xml") class EntitiesAtlasXML extends ByteArray {}
@:bitmap("assets/entities.png") class EntitiesAtlasPNG extends BitmapData { public function new() super(0, 0); }
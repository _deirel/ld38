package ru.row;

/**
 * ...
 * @author 
 */
class Action {
    public var kind(get, never):ActionKind;
    public var coolDown(get, never):Float;
    
    private var _coolDown:Float;
    private var _kind:ActionKind;

    public function new() {
    }
    
    public function execute(entity:Entity):Void {
    }
    
    private function get_kind() return _kind;
    
    private function get_coolDown() return _coolDown;
    
    public function dispose():Void {
    }
}
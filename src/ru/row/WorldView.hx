package ru.row;
import com.junkbyte.console.Cc;
import flash.Lib;
import motion.Actuate;
import motion.MotionPath;
import motion.easing.Linear;
import msignal.Signal.Signal0;
import ru.row.EntityView.TimeScaleProvider;
import ru.row.EntityView.WorldViewOffsetProvider;
import ru.row.TileMap.ShiftResult;
import ru.row.Util.PathPoint;
import ru.row.Util.Vec2f;
import ru.row.Util.Vec2i;
import ru.row.WorldView.TileViewTransition;
import starling.display.DisplayObject;
import starling.display.Quad;
import starling.display.Sprite;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

/**
 * ...
 * @author 
 */
class WorldView extends Sprite implements IPositionable {
    public var onClick:Signal0 = new Signal0();
    
    private var _metrics:Metrics;
    private var _world:World;
    private var _tilesLayer:Sprite = new Sprite();
    private var _underLayer:Sprite = new Sprite();
    private var _objectsLayer:Sprite = new Sprite();
    private var _mouseHandler:Background;
    private var _tiles:Array<TileView>;
    private var _size:Int;
    private var _entities:Map<String, EntityView>;
    private var _playerEntity:EntityView;
    private var _timeScaleProvider:TimeScaleProvider;
    private var _mousePositionNotifier:MousePositionNotifier;
    private var _playerIntention:PlayerIntention;
    private var _entityViewUnderMouse:EntityView;
    
    public function new(metrics:Metrics, model:World, tileFactory:TileFactory) {
        super();
        _world = model;
        _metrics = metrics;
        
        _underLayer.touchable = false;
        _tilesLayer.touchable = false;
        
        _mouseHandler = new Background();
        addChild(_mouseHandler);
        _mouseHandler.addEventListener(TouchEvent.TOUCH, mouseTouchHandler);
        
        _objectsLayer.addEventListener(TouchEvent.TOUCH, objectsTouchHandler);
        
        addChild(_underLayer);
        addChild(_tilesLayer);
        addChild(_objectsLayer);
        
        _size = _world.getSize();
        _tiles = [for (i in 0..._size * _size) null];
        
        for (tile in _world.getTiles()) {
            var tileView = tileFactory.create(tile.id);
            tileView.x = _metrics.getScreenX(tile.x, tile.y);
            tileView.y = _metrics.getScreenY(tile.x, tile.y);
            _tilesLayer.addChild(tileView);
            _tiles[_key(tile.x, tile.y)] = tileView;
        }
        
        sortTiles();
        
        _entities = new Map();
        _timeScaleProvider = {
            getTimeScale: function () return _world.getTimeScale()
        };
        for (e in _world.getEntities()) {
            addEntityView(e, false);
        }
        updateEntities(0.0);
        
        _world.onEntityAdded.add(entityAddedHandler);
        _world.onShift.add(shiftHandler);
        _world.onEntityDestroy.add(entityDestroyHandler);
        
        _mousePositionNotifier = new MousePositionNotifier(this);
        _mousePositionNotifier.onMousePositionChange.add(mousePositionChangeHandler);
    }
    
    private function entityAddedHandler(e:Entity):Void {
        addEntityView(e, true);
    }
    
    private function entityDestroyHandler(e:Entity):Void {
        destroyEntityView(e);
    }
    
    private function mouseTouchHandler(e:TouchEvent):Void {
        var touch = e.getTouch(_mouseHandler, TouchPhase.BEGAN);
        if (touch != null) {
            onClick.dispatch();
        }
    }
    
    private function objectsTouchHandler(e:TouchEvent):Void {
        var targetEntityView:EntityView;
        var touch = e.getTouch(_objectsLayer, TouchPhase.HOVER);
        if (touch != null) {
            targetEntityView = (cast e.target:EntityView);
            showGlowOn(targetEntityView);
            if (targetEntityView.isDeattached()) {
                _entityViewUnderMouse = null;
            } else {
                _entityViewUnderMouse = targetEntityView;
                showPathFromPlayer(_entityViewUnderMouse.getEntityPosition());
            }
        } else {
            showGlowOn(null);
            _entityViewUnderMouse = null;
        }
            
        touch = e.getTouch(_objectsLayer, TouchPhase.BEGAN);
        if (touch != null) {
            targetEntityView = (cast e.target:EntityView);
            showGlowOn(targetEntityView);
            onClick.dispatch();
        }
    }
    
    private inline function showGlowOn(targetEntityView:EntityView):Void {
        for (e in _entities) e.hideGlow();
        if (targetEntityView != null) {
            targetEntityView.showGlow();
        }
    }
    
    private function shiftHandler(shiftResult:ShiftResult, movement:Movement):Void {
        var tileViewTransitions:Array<TileViewTransition> = [];
        var movingTransitions:Array<TileViewTransition> = [];
        for (t in shiftResult.attached) {
            var tileFrom:TileView = _tiles[_key(t.from.x, t.from.y)];
            tileViewTransitions.push({ tile: tileFrom, transition: t });
        }
        for (t in shiftResult.unattached) {
            var tileFrom:TileView = _tiles[_key(t.from.x, t.from.y)];
            var tileViewTransition:TileViewTransition = { tile: tileFrom, transition: t };
            tileViewTransitions.push(tileViewTransition);
            movingTransitions.push(tileViewTransition);
        }
        for (t in tileViewTransitions) {
            _tiles[_key(t.transition.to.x, t.transition.to.y)] = t.tile;
        }
        animateTransitions(movingTransitions, movement);
    }
    
    private function animateTransitions(movingTransitions:Array<TileViewTransition>, movement:Movement):Void {
        var i = 0;
        Random.shuffle(movingTransitions);
        for (t in movingTransitions) {
            Actuate.timer(0.025 * (i++)).onComplete(animateTileTransition, [t]);
        }
    }
    
    public function writeMousePosition(result:Vec2i):Void {
        var mouseX:Float = Lib.current.stage.mouseX - this.x;
        var mouseY:Float = Lib.current.stage.mouseY - this.y;
        var minDist:Float = 100000;
        var selectedTileId:Int = -1;
        for (i in 0..._tiles.length) {
            var t = _tiles[i];
            var dist = Math.abs(mouseX - t.x) + Math.abs(mouseY - t.y);
            if (dist < minDist) {
                minDist = dist;
                selectedTileId = i;
            }
        }
        if (selectedTileId > -1) {
            result.x = _world.getOffsetX() + selectedTileId % _size;
            result.y = _world.getOffsetY() + Std.int(selectedTileId / _size);
        }
    }
    
    private function mousePositionChangeHandler(pos:Vec2i):Void {
        if (_entityViewUnderMouse == null) {
            showPathFromPlayer(pos);
        } else {
            showPathFromPlayer(_entityViewUnderMouse.getEntityPosition());
        }
    }
    
    private function showPathFromPlayer(to:Vec2i):Void {
        var path = _world.getPathFromPlayer(to.x, to.y);
        showPath(path);
        if (path.length > 0) {
            var lastPoint = path[path.length - 1];
            _playerIntention = lastPoint.monster != null 
                    ? PlayerIntention.Attack(lastPoint.monster) 
                    : PlayerIntention.MoveToPoint(lastPoint.x + _world.getOffsetX(), lastPoint.y + _world.getOffsetY());
        } else {
            _playerIntention = null;
        }
    }
    
    private function showPath(path:Array<PathPoint>):Void {
        var tilesWithMarkerIds:Map<Int,String> = new Map();
        for (p in path) {
            var id = _key(p.x, p.y);
            tilesWithMarkerIds[id] = p.monster;
        }
        for (i in 0..._tiles.length) {
            var isMarker:Bool = tilesWithMarkerIds.exists(i);
            var monsterId:String = tilesWithMarkerIds[i];
            _tiles[i].showPathMarker(isMarker, monsterId != null);
        }
    }
    
    private function animateTileTransition(t:TileViewTransition):Void {
        _underLayer.addChild(t.tile);
        var globalXTo = _world.getOffsetX() + t.transition.to.x;
        var globalYTo = _world.getOffsetY() + t.transition.to.y;
        var xTo = _metrics.getScreenX(globalXTo, globalYTo);
        var yTo = _metrics.getScreenY(globalXTo, globalYTo);
        
        var path = new MotionPath();
        path.line(t.tile.x, t.tile.y, 0);
        path.bezier(xTo, yTo, 0.5 * (t.tile.x + xTo), 0.5 * (t.tile.y + yTo) + 200);
        
        t.tile.changeTo(t.transition.targetTileId, 0.3);
        Actuate.motionPath(t.tile, 0.3, { 
            x: path.x,
            y: path.y
        }).onComplete(tileTransitionCompleteHandler, [t.tile]).onUpdate(sortUnderTiles).ease(Linear.easeNone);
    }
    
    private function addEntityView(e:Entity, needToUpdateEntities:Bool):Void {
        var entityView = new EntityView(e, _timeScaleProvider);
        _objectsLayer.addChild(entityView);
        _entities[e.id] = entityView;
        if (e == _world.getPlayer()) {
            _playerEntity = entityView;
        }
        if (needToUpdateEntities) {
            updateEntities(0.0);
        }
        entityView.attach(0.2, 0.15);
    }
    
    private function destroyEntityView(e:Entity):Void {
        var entityView = _entities[e.id];
        if (entityView != null) {
            _entities.remove(e.id);
            entityView.deattach(0.2);
            if (_entityViewUnderMouse == entityView) {
                _entityViewUnderMouse = null;
                showPathFromPlayer(_mousePositionNotifier.getPosition());
            }
        }
    }
    
    private inline function _key(x:Int, y:Int):Int return y * _size + x;
    
    private function sortTiles():Void {
        _tilesLayer.sortChildren(sortFunction);
    }
    
    private function sortUnderTiles():Void {
        _underLayer.sortChildren(sortFunction);
    }
    
    private function tileTransitionCompleteHandler(tileView:TileView):Void {
        _tilesLayer.addChild(tileView);
        sortTiles();
    }
    
    public function writePlayerScreenPosition(pos:Vec2f):Void {
        pos.x = _metrics.getScreenX(_playerEntity.realtimeX, _playerEntity.realtimeY); 
        pos.y = _metrics.getScreenY(_playerEntity.realtimeX, _playerEntity.realtimeY); 
    }
    
    public function update(dt:Float):Void {
        _world.update(dt);
        updateEntities(dt);
        _objectsLayer.sortChildren(sortFunction);
        _mousePositionNotifier.update();
    }
    
    public function setX(value:Float):Void { this.x = value; _mouseHandler.x = -value; }
    
    public function setY(value:Float):Void { this.y = value; _mouseHandler.y = -value; }
    
    private inline function updateEntities(dt:Float):Void {
        var gameDt:Float = dt * _timeScaleProvider.getTimeScale();
        var entities = _world.getEntities();
        for (entityView in _entities) {
            entityView.update();
            entityView.x = _metrics.getScreenX(entityView.realtimeX, entityView.realtimeY);
            entityView.y = _metrics.getScreenY(entityView.realtimeX, entityView.realtimeY);
        }
    }
    
    private static function sortFunction(tile1:DisplayObject, tile2:DisplayObject):Int {
        return tile1.y > tile2.y ? 1 : tile1.y < tile2.y ? -1 : 0;
    }
    
    public function getMousePositionNotifier():MousePositionNotifier return _mousePositionNotifier;
    
    public function getPlayerIntention():PlayerIntention return _playerIntention;
}

typedef TileViewTransition = {
    var tile:TileView;
    var transition:TileTransition;
}
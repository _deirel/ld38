package ru.row;
import msignal.Signal.Signal2;

/**
 * ...
 * @author 
 */
@:generic class TrackingValue<T> {
    public var _(get, set):T;
    public var onChange:Signal2<T,T> = new Signal2();
    
    private var _owner:Dynamic;
    private var _name:String;
    
    public function new(owner:Dynamic, name:String) {
        _owner = owner;
        _name = name;
    }
    
    public function set__(value:T):T {
        var prevValue = this._;
        Reflect.setProperty(_owner, _name, value);
        if (value != prevValue) {
            onChange.dispatch(prevValue, value);
        }
        return value;
    }
    
    public function get__() return Reflect.field(_owner, _name);
    
    public function dispose():Void {
        onChange.removeAll();
        onChange = null;
        _owner = null;
    }
}
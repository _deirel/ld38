package ru.row;
import com.junkbyte.console.Cc;
import motion.Actuate;
import motion.easing.Linear;
import ru.row.EntityView.WorldViewOffsetProvider;
import starling.display.Image;
import starling.display.Sprite;
import starling.filters.GlowFilter;

/**
 * ...
 * @author 
 */
class EntityView extends Sprite {
    private static var _graphicsProvider:ObjectGraphicsProvider = new ObjectGraphicsProvider();
        
    public var realtimeX:Float;
    public var realtimeY:Float;
    
    private var _image:Image;
    private var _glowFilter:GlowFilter;
    
    private var _entity:Entity;
    private var _lastAction:Action;
    private var _timeScaleProvider:TimeScaleProvider;
    
    private var _action:ActionView;
    
    private var _inJump:Bool = false;
    
    private var _isDeattached:Bool = false;
    
    public function new(entity:Entity, timeScaleProvider:TimeScaleProvider) {
        super();
        
        _timeScaleProvider = timeScaleProvider;
        _entity = entity;
        _entity.onBeginAction.add(beginActionHandler);
        _entity.onEndAction.add(endActionHandler);
        
        realtimeX = _entity.x;
        realtimeY = _entity.y;
        
        _image = _graphicsProvider.getGraphics(_entity.type.type);
        _image.pivotX = 64 - _entity.type.pivotX;
        _image.pivotY = _image.height - (_entity.type.pivotY == null ? 0.0 : _entity.type.pivotY);
        addChild(_image);
        
        _image.y = -128;
        _image.alpha = 0.0;
        
        if (_entity.isInteractive() && !_entity.isPlayer()) {
            touchGroup = true;
        } else {
            touchable = false;
        }
    }
    
    private function endActionHandler(action:Action):Void {
        Cc.log("end action " + action + " " + (_entity.id));
        if (_lastAction != null) {
            freeAction();
        }
    }
    
    private function beginActionHandler(action:Action):Void {
        Cc.log("action " + action + " " + (_entity.id));
        if (_inJump) {
            realtimeX = _entity.x;
            realtimeY = _entity.y;
        }
        switch (action.kind) {
            case ActionKind.Move(dx, dy):
                setActionView(new ActionViewMove(this, _entity.x, _entity.y));
                
            case ActionKind.Attack(enemyId):
                setActionView(new ActionViewAttack(this, _entity.world.getEntity(enemyId)));
                
            default:
                setActionView(null);
        }
    }
    
    public function showGlow():Void {
        if (_glowFilter == null) {
            _glowFilter = new GlowFilter();
        }
        filter = _glowFilter;
    }
    
    public function hideGlow():Void {
        filter = null;
    }
    
    private function setActionView(action:ActionView):Void {
        freeAction();
        _action = action;
        if (_action != null) {
            _action.setProgress(0.0);
        }
    }
    
    public function update():Void {
        if (_action != null && _entity.lastAction != null) {
            var p:Float = 1 - _entity.getCooldownElapsed() / _entity.lastAction.coolDown;
            _action.setProgress(p);
        }
    }
    
    public function reattach(globalX:Int, globalY:Int, duration:Float):Void {
        freeAction();
        _inJump = true;
        Actuate.tween(_image, duration / 2, { y: -128, alpha: 0.0 }).ease(Linear.easeNone).onComplete(function () {
            realtimeX = globalX;
            realtimeY = globalY;
            _inJump = false;
            freeAction();
            Actuate.tween(_image, duration / 2, { y: 0, alpha: 1.0 }).ease(Linear.easeNone);
        });
    }
    
    public function deattach(duration:Float):Void {
        _isDeattached = true;
        touchable = false;
        freeAction();
        freeEntity();
        Actuate.tween(_image, duration, { y: -128, alpha: 0.0 }).ease(Linear.easeNone).onComplete(function () {
            removeFromParent(true);
        });
    }
    
    public function attach(delay:Float, duration:Float):Void {
        Actuate.tween(_image, duration, { y: 0, alpha: 1.0 }).ease(Linear.easeNone).delay(delay);
    }
    
    private function freeAction():Void {
        if (_action != null) {
            _action.dispose();
            _action = null;
        }
    }
    
    public function setImageYOffset(value:Float):Void {
        if (_image != null) {
            _image.y = value;
        }
    }
    
    public function isDeattached() return _isDeattached;
    
    override public function dispose():Void {
        _glowFilter = null;
        _image = null;
        freeEntity();
        if (_action != null) {
            _action.dispose();
            _action = null;
        }
        super.dispose();
    }
    
    private inline function freeEntity():Void {
        if (_entity != null) {
            _entity.onBeginAction.remove(beginActionHandler);
            _entity = null;
        }
    }
    
    public function getEntity() return _entity;
    
    public function getEntityPosition() return { x: _entity.x, y: _entity.y };
}

typedef TimeScaleProvider = {
    function getTimeScale():Float;
}

typedef WorldViewOffsetProvider = {
    function getOffsetX():Int;
    function getOffsetY():Int;
}
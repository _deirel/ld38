package ru.row;
import com.junkbyte.console.Cc;
import ru.row.Action;
import ru.row.Util.Vec2i;

/**
 * ...
 * @author 
 */
class GlobalGoalMoveToPoint extends GlobalGoal {
    private var _world:World;
    private var _x:Int;
    private var _y:Int;
    private var _cachedPath:Array<Vec2i>;
    private var _entity:Entity;
    
    public function new(world:World, x:Int, y:Int, entity:Entity) {
        super();
        _world = world;
        _entity = entity;
        _x = x;
        _y = y;
    }
    
    override public function getNextAction():Action {
        if (_cachedPath == null) {
            _cachedPath = _world.getPathFromPlayer(_x, _y);
        }
        if (_cachedPath.length > 1) {
            var p0 = _cachedPath.shift();
            var p1 = _cachedPath[0];
            
            if (_world.isWalkable(p1.x, p1.y)) {
                return new ActionMoveNextTile(p1.x - p0.x, p1.y - p0.y, _entity);
            } else {
                _cachedPath = null;
                return getNextAction();
            }
        }
        return null;
    }
    
    override public function dispose():Void {
        _world = null;
        _cachedPath = null;
        _entity = null;
        super.dispose();
    }
}
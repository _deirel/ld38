package ru.row;
import pathfinder.IMap;

/**
 * ...
 * @author 
 */
class PathMap implements IMap {
	public var rows(default, null):Int;
	public var cols(default, null):Int;
    
    private var _world:World;
    
    public function new(world:World) {
        _world = world;
        rows = _world.getSize();
        cols = _world.getSize();
    }
    
    public function isWalkable(p_x:Int, p_y:Int):Bool {
        return _world.isWalkable(p_x, p_y);
    }
}
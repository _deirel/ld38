package ru.row;
import flash.Lib;
import haxe.Json;
import haxe.Timer;
import motion.Actuate;
import motion.easing.Linear;
import ru.row.Util.Vec2f;
import ru.row.Util.Vec2i;
import starling.core.Starling;
import starling.display.Sprite;
import starling.events.EnterFrameEvent;
import starling.textures.Texture;
import starling.textures.TextureAtlas;
import starling.utils.AssetManager;
import starlingbuilder.engine.DefaultAssetMediator;
import starlingbuilder.engine.IAssetMediator;
import starlingbuilder.engine.UIBuilder;
import flash.utils.ByteArray;
import flash.display.BitmapData;
import flash.xml.XML;

/**
 * ...
 * @author 
 */
class App extends Sprite implements IScreenSizeProvider {
    public static var instance:App;
    
    public static var stageWidth:Int;
    public static var stageHeight:Int;
    
    public static var assetManager:AssetManager;
    public static var uiBuilder:UIBuilder;
    
    public function new() {
        super();
        
        Starling.current.showStatsAt("left", "bottom");
        
        instance = this;
        
        stageWidth = Lib.current.stage.stageWidth;
        stageHeight = Lib.current.stage.stageHeight;
        
        assetManager = new AssetManager();
        assetManager.addTextureAtlas("ui", new TextureAtlas(Texture.fromBitmapData(new HUDBd()), new XML(new HUDXml())));
        
        uiBuilder = new UIBuilder(new DefaultAssetMediator(assetManager));
        
        // ----
        
        var game:GameView = new GameView();
        addChild(game);
    }
    
    public function getScreenWidth():Int {
        return stageWidth;
    }
    
    public function getScreenHeight():Int {
        return stageHeight;
    }
}

@:file("assets/ui.xml") class HUDXml extends ByteArray {}
@:bitmap("assets/ui.png") class HUDBd extends BitmapData { public function new () super(0, 0); }
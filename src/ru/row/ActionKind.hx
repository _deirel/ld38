package ru.row;

/**
 * @author 
 */
enum ActionKind {
    Move(dx:Int, dy:Int);
    Attack(id:String);
}
package ru.row;

/**
 * ...
 * @author 
 */
class HUDMediator extends BaseMediator<HUDView> {
    private var _world:World;
    private var _game:GameView;
    
    public function new(view:HUDView, world:World, game:GameView) {
        super(view);
        _view.onSkillsClick.add(btnSkillsClickHandler);
        _world = world;
        _game = game;
        
        var player = _world.getPlayer();
        player.onHPChanged.add(playeHPCHangedHandler);
        _view.setHP(player.hp, player.maxHP);
        
        _world.getEXPSystem().onChange.add(expChangeHandler);
        _view.setExp(_world.getEXPSystem().exp);
    }
    
    // ------------------- HANDLERS ------------------------
    
    private function btnSkillsClickHandler():Void {
        _game.showStatsDialog();
    }
    
    private function playeHPCHangedHandler(player:Entity):Void {
        _view.setHP(player.hp, player.maxHP);
    }
    
    private function expChangeHandler(sys:EXPSystem):Void {
        _view.setExp(sys.exp);
    }
    
    // ------------------- UTIL METHODS --------------------
    
    override public function dispose():Void {
        if (_world != null) {
            var player = _world.getPlayer();
            if (player != null) {
                player.onHPChanged.remove(playeHPCHangedHandler);
            }
            _world.getEXPSystem().onChange.remove(expChangeHandler);
        }
        _game = null;
        super.dispose();
    }
}
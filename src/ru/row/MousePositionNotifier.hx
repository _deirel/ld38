package ru.row;
import msignal.Signal.Signal1;
import ru.row.Util.Vec2i;

/**
 * ...
 * @author 
 */
class MousePositionNotifier {
    public var onMousePositionChange:Signal1<Vec2i> = new Signal1();
    
    private var _world:WorldView;
    private var _mousePosition:Vec2i = { x: 0, y: 0 };
    private var _prevMousePosition:Vec2i = { x: 0, y: 0 };
    private var _counter:Float = 0;
    private var _updateRate:Int;
    
    public function new(world:WorldView, updateRate:Int = 3) {
        _world = world;
        _updateRate = updateRate;
        _world.writeMousePosition(_mousePosition);
    }
    
    public function update():Void {
        _counter++;
        if (_counter >= _updateRate) {
            _counter -= _updateRate;
            _prevMousePosition.x = _mousePosition.x;
            _prevMousePosition.y = _mousePosition.y;
            _world.writeMousePosition(_mousePosition);
            if (_mousePosition.x != _prevMousePosition.x || _mousePosition.y != _prevMousePosition.y) {
                onMousePositionChange.dispatch(_mousePosition);
            }
        }
    }
    
    public function getPosition() return { x: _mousePosition.x, y: _mousePosition.y };
}
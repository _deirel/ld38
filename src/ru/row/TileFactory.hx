package ru.row;

/**
 * @author 
 */
typedef TileFactory = {
    function create(id:String):TileView;
}
package ru.row;

/**
 * ...
 * @author 
 */
class MapGenerator {
    public var world:World;
    
    private var _monstersTotalWeight:Float;
    private var _monsters:Array<{ w:Float, type:EntityType }> = [
        { w: 5, type: EntityType.Cat },
        { w: 1, type: EntityType.Minotaur },
        { w: 2, type: EntityType.Heal },
        { w: 0.5, type: EntityType.Portal }
    ];
    
    public function new() {
        _monstersTotalWeight = Lambda.fold(_monsters, function (w, acc:Float):Float return acc + w.w, 0.0);
        _monsters.sort(function (w1, w2) return w1.w < w2.w ? -1 : w1.w > w2.w ? 1 : 0);
    }

    public function createStartArea(size:Int):Array<Tile> {
        return [for (x in 0...size) for (y in 0...size) new Tile(x, y, getRandomTileId())];
    }
    
    private function getRandomTileId():String{
        return Random.fromArray(TileId.All);
    }
    
    private function getRandomMonsterType():EntityType {
        var i = Random.float(0.0, _monstersTotalWeight);
        for (w in _monsters) if (i < w.w) return w.type; else i -= w.w;
        return null;
    }
    
    public function completeTiles(transitions:Array<TileTransition>, monsters:Map<String,Entity>):Void {
        for (t in transitions) {
            var coords = '${t.from.x}:${t.from.y}';
            var monster = monsters[coords];
            t.targetTileId = monster != null ? t.sourceTileId : getRandomTileId();
            if (monster != null) {
                t.targetEntity = Instance(monster);
            }
        }
    }
    
    public function completeEntitites(transitions:Array<TileTransition>):Void {
        if (world.getEnemyCount() < 4 && Math.random() > 0.8) {
            Random.fromArray(transitions).targetEntity = Id(getRandomMonsterType());
        }
        for (t in transitions) {
            if (t.targetEntity == null && Math.random() > 0.8) {
                t.targetEntity = Id(Random.fromArray(EntityType.Decorations));
            }
        }
    }
}    

typedef MapCellData = {
    var tileId:String;
    var entity:EntityInstance;
}

enum EntityInstance {
    Id(type:EntityType);
    Instance(entity:Entity);
}
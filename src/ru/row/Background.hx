package ru.row;
import starling.display.Image;
import starling.display.Sprite;

/**
 * ...
 * @author asdfsafd
 */
class Background extends Sprite {
    
    public function new() {
        super();
        touchGroup = true;
        addChild(new Image(TileGraphicsProvider.getBGTexture()));
        addChild(new Image(TileGraphicsProvider.getCloudsTexture()));
    }
}
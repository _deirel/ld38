package ru.row;

/**
 * ...
 * @author 
 */
class ActionViewMove extends ActionView {
    private var _startX:Float;
    private var _startY:Float;
    private var _targetX:Float;
    private var _targetY:Float;
    
    public function new(entity:EntityView, x:Int, y:Int) {
        super(entity);
        _startX = _entity.realtimeX;
        _startY = _entity.realtimeY;
        _targetX = x;
        _targetY = y;
    }
    
    override public function setProgress(p:Float):Void {
        if (p < 0) {
            p = 0;
        } else if (p > 1) {
            p = 1;
        }
        _entity.realtimeX = p * _targetX + (1 - p) * _startX;
        _entity.realtimeY = p * _targetY + (1 - p) * _startY;
    }
    
    override public function end():Void {
        _entity.realtimeX = _targetX;
        _entity.realtimeY = _targetY;
        super.end();
    }
}
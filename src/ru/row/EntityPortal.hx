package ru.row;
import ru.row.GlobalGoal;

/**
 * ...
 * @author 
 */
class EntityPortal extends EntityCreature {

    public function new(id:String, x:Int, y:Int) {
        super(id, EntityType.Portal, x, y);
    }

    override function getNewGlobalGoal():GlobalGoal {
        return null;
    }
    
    override function onDeathHandler():Void {
        world.upgradeLevel();
        super.onDeathHandler();
    }
}
package ru.row;
import ru.row.GlobalGoal;

/**
 * ...
 * @author 
 */
class EntityCreature extends Entity {

    public function new(id:String, type:EntityType, x:Int, y:Int) {
        super(id, type, x, y);
    }

    override function getNewGlobalGoal():GlobalGoal {
        return new GlobalGoalAttack(world, this, world.getPlayer());
    }
}
package ru.row;
import msignal.Signal.Signal0;
import starling.display.DisplayObject;
import starling.display.Sprite;
import flash.utils.ByteArray;
import starling.text.TextField;

using ru.row.Util;

/**
 * ...
 * @author 
 */
class HUDView extends BaseView {
    public var onSkillsClick:Signal0 = new Signal0();
    
    private var _hpLabel:TextField;
    private var _hpMask:DisplayObject;
    
    private var _expLabel:TextField;
    
    public function new() {
        super();
        graphics = App.uiBuilder.create(EmbedJson.parse(new HUDJson()));
        
        var btnSkills = _d(new SimpleButton(graphics.getChildByName("skills")));
        btnSkills.onClick.add(btnSkillsHandler);
        
        _hpLabel = graphics.cc("health").ct("healthLabel");
        _hpMask = graphics.cc("health").ch("healthBar").mask;
        
        _expLabel = graphics.cc("exp").ct("expLabel");
    }
    
    private function btnSkillsHandler(_):Void {
        onSkillsClick.dispatch();
    }
    
    public function setHP(value:Float, max:Float):Void {
        _hpLabel.text = Std.string(Std.int(value));
        _hpMask.x = -_hpMask.width + (value / max) * _hpMask.width;
    }
    
    public function setExp(value:Float):Void {
        _expLabel.text = Std.string(Std.int(value));
    }
    
    override public function dispose():Void {
        _hpLabel = null;
        _hpMask = null;
        _expLabel = null;
        super.dispose();
    }
}

@:file("assets/hud.json") class HUDJson extends ByteArray {}
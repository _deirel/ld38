package ru.row;
import starling.display.DisplayObject;
import starling.display.DisplayObjectContainer;
import starling.text.TextField;

/**
 * ...
 * @author 
 */
typedef Vec2f = {
    var x:Float;
    var y:Float;
}

typedef Vec2i = {
    var x:Int;
    var y:Int;
}

typedef PathPoint = {
    var x:Int;
    var y:Int;
    var monster:String;
}

class Util {
    
    public static function ct(container:DisplayObjectContainer, name:String):TextField {
        return cast container.getChildByName(name);
    }
    
    public static function ch(container:DisplayObjectContainer, name:String):DisplayObject {
        return cast container.getChildByName(name);
    }
    
    public static function cc(container:DisplayObjectContainer, name:String):DisplayObjectContainer {
        return cast container.getChildByName(name);
    }
}
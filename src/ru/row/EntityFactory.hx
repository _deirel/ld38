package ru.row;

/**
 * ...
 * @author 
 */
class EntityFactory {
    private static var _uidCounter:Int = 0;
    
    public function new() {
    }
    
    public function create(type:EntityType, x:Int, y:Int):Entity {
        return switch (type.kind) {
            case Player: new EntityPlayer(x, y);
            case Portal: new EntityPortal(uid(), x, y);
            case Heal: new EntityHeal(uid(), x, y);
            case Monster: new EntityCreature(uid(), type, x, y);
            case Decoration: new EntityDecoration(uid(), type, x, y);
        }
    }
    
    private inline function uid() return 'entity_${_uidCounter++}';
}
package ru.row;
import ru.row.Util.Vec2f;
import ru.row.Util.Vec2i;
import starling.display.Sprite;
import starling.events.EnterFrameEvent;

/**
 * ...
 * @author asdfsafd
 */
class GameView extends Sprite {
    private var _statsDialog:StatsView;
    private var _paused:Bool = false;
    private var _hud:HUDMediator;
    private var _world:World;
    
    public function new() {
        super();
        
        _world = new World(7, new MapGenerator(), new EntityFactory());
        
        var metrics = new Metrics(66, 39);
        
        var worldView = new WorldView(metrics, _world, {
            create: function (id:String) return new TileView(id)
        });
        
        var hudView = new HUDView();
        _hud = new HUDMediator(hudView, _world, this);
        
        addChild(worldView);
        addChild(hudView.graphics);
        
        var camera = new Camera(worldView, App.instance);
        
        var playerPos:Vec2f = { x: 0, y: 0 };
        var mousePos:Vec2i = { x: 0, y: 0 };
        
        addEventListener(EnterFrameEvent.ENTER_FRAME, function (e:EnterFrameEvent) {
            if (_paused) {
                return;
            }
            worldView.update(e.passedTime);
            worldView.writePlayerScreenPosition(playerPos);
            camera.x = playerPos.x;
            camera.y = playerPos.y;
        });
        
        worldView.onClick.add(function () {
            if (_paused) {
                return;
            }
            var intention = worldView.getPlayerIntention();
            var goal:GlobalGoal = intention == null ? null : switch (intention) {
                case PlayerIntention.Attack(targetEntityId):
                    new GlobalGoalAttack(_world, _world.getPlayer(), _world.getEntity(targetEntityId));
                case PlayerIntention.MoveToPoint(x, y):
                    new GlobalGoalMoveToPoint(_world, x, y, _world.getPlayer());
                default: null;
            }
            if (goal != null) {
                _world.getPlayer().setGlobalGoal(goal);
            }
        });
        
        //world.addEntity(new EntityFactory().create(EntityType.Cat, 0, 0));
        //world.addEntity(new EntityFactory().create(EntityType.Cat, 6, 0));
        //world.addEntity(new EntityFactory().create(EntityType.Minotaur, 0, 6));
        //world.addEntity(new EntityFactory().create(EntityType.Minotaur, 0, 5));
        //world.addEntity(new EntityFactory().create(EntityType.Minotaur, 0, 4));
    }
    
    public function pause():Void {
        _paused = true;
        _hud.pause();
    }
    
    public function resume():Void {
        _paused = false;
        _hud.resume();
    }

    public function showStatsDialog():Void {
        if (_statsDialog != null) {
            return;
        }
        _statsDialog = new StatsView(_world);
        _statsDialog.graphics.x = (App.stageWidth - _statsDialog.graphics.width) / 2;
        _statsDialog.graphics.y = (App.stageHeight - _statsDialog.graphics.height) / 2;
        addChild(_statsDialog.graphics);
        _statsDialog.onClose.add(statsDialogCloseHandler);
        pause();
    }
    
    private function statsDialogCloseHandler():Void {
        _statsDialog.dispose();
        _statsDialog = null;
        resume();
    }
}
package ru.row;

/**
 * ...
 * @author 
 */
class Metrics {
    private var _iX:Float;
    private var _iY:Float;
    
    public function new(indentX:Float, indentY:Float) {
        _iX = indentX;
        _iY = indentY;
    }
    
    public function getScreenX(x:Float, y:Float) return (y - x) * _iX;
    
    public function getScreenY(x:Float, y:Float) return (y + x) * _iY;
}
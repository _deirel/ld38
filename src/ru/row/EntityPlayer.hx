package ru.row;
import ru.row.GlobalGoal;

/**
 * ...
 * @author 
 */
class EntityPlayer extends EntityCreature {
    public static inline var ID = "player";
    
    public function new(x:Int, y:Int) {
        super(ID, EntityType.Player, x, y);
    }
    
    override function getNewGlobalGoal():GlobalGoal {
        return null;
    }
}
package ru.row;
import starling.display.Sprite;

/**
 * ...
 * @author asdfsafd
 */
class BaseView {
    public var graphics:Sprite;
    
    private var _disposer:Disposer;
    
    public function new() {
        _disposer = new Disposer();
    }
    
    private function _d<T>(obj:T):T {
        _disposer.add(obj);
        return obj;
    }
    
    public function pause() graphics.touchable = false;
    
    public function resume() graphics.touchable = true;
    
    public function dispose():Void {
        _disposer.dispose();
        _disposer = null;
        graphics.removeFromParent(true);
        graphics = null;
    }
}
package ru.row;
import ru.row.SRandom.RandomTemplate;
import ru.row.Stat;

/**
 * ...
 * @author asdfsafd
 */
class Stats {
    private var _damage:RandomTemplate;
    private var _statMap:Map<Stat,Float>;       // Базовые значения
    
    private var _owner:Entity;
    private var _isPlayer:Bool;
    
    private var _healBase:RandomTemplate = SRandom.fromString("3d5");
    
    public function new(owner:Entity, damage:RandomTemplate, stamina:Float, strength:Float, speed:Float, isPlayer:Bool) {
        _owner = owner;
        _isPlayer = isPlayer;
        _damage = damage;
        _statMap = [
            Stamina => stamina,
            Strength => strength,
            Speed => speed
        ];
    }
    
    public function getStatValue(stat:Stat) {
        return _isPlayer ? _statMap[stat] : 100 * Math.pow(1.2, getLevel() - 1) - 100;
    }
    
    public function upgradeStat(stat:Stat) {
        _statMap[stat]++;    
        switch (stat) {
            case Stamina:
                var prevValue = _owner.maxHP;
                _owner.maxHP = getHP();
                _owner.hp += (_owner.maxHP - prevValue);
            default:
        }
    }
    
    public function getStatUpgradeCost(stat:Stat) return 10 * Math.pow(2, (getStatValue(stat) - 10) / 10);
    
    public function getKillExperience() return _owner.type.reward * Math.pow(2, getLevel() - 1);
    
    public function getDamage() return SRandom.getInt(_damage) * (1 + getStatValue(Strength) / 100);
    
    public function getHP() return _owner.baseHP * (1 + getStatValue(Stamina) / 100);
    
    public function getHeal() return SRandom.getInt(_healBase) * Math.pow(1.2, getLevel() - 1);
    
    public function getMoveCooldown() return _owner.type.moveCoolDown * (1 - getStatValue(Speed) / (getStatValue(Speed) + 100));
    
    public function getAttackCooldown() return _owner.type.attackCoolDown * (1 - getStatValue(Speed) / (getStatValue(Speed) + 100));
    
    private function getLevel() return _owner.world.getLevel();
    
    public function clone(owner:Entity):Stats {
        var stats = new Stats(owner, _damage, _statMap[Stamina], _statMap[Strength], _statMap[Speed], _isPlayer);
        //stats.level = this.level;
        return stats;
    }
    
    public function dispose():Void {
        _owner = null;
    }
}